#importing libraries
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.animation import FuncAnimation
import matplotlib.animation as animation
import pandas as pd
import time

INTERVAL = 1000
DATASET = "sm_sp_z_2021-05-12-16-03-52"
X = "time"
Y1 = "acceleration_x"
Y2 = "acceleration_y"
Y3 = "acceleration_z"
X_LABEL = "time (s)"
Y_LABEL = "acceleration (g)"
TITLE = "Acceleration"

df = pd.read_csv("./"+ DATASET + ".csv")
totalFrames = 1000 #max(df.index)

plt.style.use('seaborn-whitegrid')
plt.rcParams["figure.figsize"] = 6,4
fig = plt.figure()
ax1 = fig.add_subplot(1,1,1)
plt.subplots_adjust(right=0.7)


last_index = 0
xs = []
ys1 = []
ys2 = []
ys3 = []
startTime = time.time()

def animate(i): 
    global last_index

    j = 0
        
    currentTime = df.loc[last_index, X]
    currentValueX = df.loc[last_index, Y1]
    currentValueY = df.loc[last_index, Y2]
    currentValueZ = df.loc[last_index, Y3]
    
    while currentTime < (i)*INTERVAL*0.001 and last_index+j < totalFrames:

        xs.append(currentTime)
        ys1.append(currentValueX)
        ys2.append(currentValueY)
        ys3.append(currentValueZ)


        currentTime = df.loc[last_index+j, X]
        currentValueX = df.loc[last_index+j, Y1]
        currentValueY = df.loc[last_index+j, Y2]
        currentValueZ = df.loc[last_index+j, Y3]

        #print(currentTime, currentValue, time.time()-startTime)
        j += 1

    last_index = last_index+j
    
    ax1.clear()
    
    if(currentTime-10) > 0:
        ax1.set_xlim(currentTime-10, currentTime)
    

    line1, = ax1.plot(xs, ys1, label = Y1)
    line2, = ax1.plot(xs, ys2, label = Y2)
    line3, = ax1.plot(xs, ys3, label = Y3)
    ax1.set_xlabel(X_LABEL)
    ax1.set_ylabel(Y_LABEL)
    ax1.set_title(TITLE)
    ax1.legend(handles = [line1, line2, line3], bbox_to_anchor=(1.04,1), borderaxespad=0)



anim = animation.FuncAnimation(fig, animate, interval=INTERVAL, frames=totalFrames, repeat = False)

#writervideo = animation.FFMpegWriter(fps=1) 
#anim.save(DATASET + "_" + TITLE + ".mp4", writer = "ffmpeg", fps = 1)

plt.show()