#Data time,magnetometer_z, magnetometer_x,magnetic_variation,magnetometer_y,magnetometer_norm
#rotation_speed_y,rotation_speed_x,rotation_speed_z,
#acceleration_y,acceleration_z,acceleration_x,
#temperature,pressure,height,
#,meter_per_second_over_ground
#longitude,latitude,course_made_good,utc_time,speed_z,
import dash
import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output, State
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import plotly.graph_objects as go
import plotly.express as px
from plotly.subplots import make_subplots
import pandas as pd
import glob
from colors import colors


EXCLUDED_COLUMNS_FOR_GENERAL_DATA = ["NRW", "checksum", "NS", "EW"]


app = dash.Dash(external_stylesheets=[dbc.themes.DARKLY])
#Reading files for diffrent dataframes
path = r'./sensor_data/'
file_paths = glob.glob(path + '/*.csv')


print("\n")
print("---------------------- DATA VISUALIZATION ----------------------")
print("\n")

if len(file_paths) == 0:
    print("No files recognized!")
    exit()

print("Recognized files: ")
for f in file_paths:
    print("- " + f )
current_path = "/"

#Get file names
file_names = []
for file in file_paths:
    splitted = file.split('/')
    file_names.append(splitted[-1])

# Default dataframe is set to the last element of the file paths
global df
df = pd.read_csv(file_paths[len(file_paths)-1])

# Setting dropdown options 
option = []
opt = {}
for i in range(len(file_paths)):
    opt['label'] = file_names[i] 
    opt['value'] = file_paths[i]
    option.append(opt.copy())


def create_map(df):
    """" Creates map for GPS-Data visualization."""
    fig = px.scatter_mapbox(df, lat="latitude", lon="longitude", zoom=15,  color="time")
    fig.update_layout(mapbox_style="open-street-map", plot_bgcolor=colors['background'],paper_bgcolor=colors['background'], font_color=colors['text'])
    fig.update_layout(margin={"r":0,"t":0,"l":0,"b":0})
    fig.update_layout(height=700)
    return fig

def create_sensor_fig(df):
    """ Creates a Graph for sensor data with a rangeslider.
    
     5 subplots for acceleration, magnetometer, gyroscope, temperature and height.
     Acceleration, magnetometer and gyroscope subplots have traces for X, Y and Z direction.
     """
    fig = make_subplots(rows = 5, cols = 1, shared_xaxes=True, subplot_titles=("Acceleration", "Magnetometer", "Gyro", "Temperature", "Height"))

    fig.add_trace(go.Scatter(x=df['time'], y=df['acceleration_x'],
                        mode='lines',
                        name='Acceleration X'), row = 1, col = 1)
    fig.add_trace(go.Scatter(x=df['time'], y=df['acceleration_y'],
                        mode='lines',
                        name='Acceleration Y'), row = 1, col = 1)
    fig.add_trace(go.Scatter(x=df['time'], y=df['acceleration_z'],
                        mode='lines',
                        name='Acceleration Z'), row = 1, col = 1)

    fig.add_trace(go.Scatter(x=df['time'], y=df['magnetometer_x'],
                        mode='lines',
                        name='Magnetometer X'), row = 2, col = 1)
    fig.add_trace(go.Scatter(x=df['time'], y=df['magnetometer_y'],
                        mode='lines',
                        name='Magnetometer Y'), row = 2, col = 1)
    fig.add_trace(go.Scatter(x=df['time'], y=df['magnetometer_z'],
                        mode='lines',
                        name='Magnetometer Z'), row = 2, col = 1)

    fig.add_trace(go.Scatter(x=df['time'], y=df['magnetometer_norm'],
                        mode='lines',
                        name='Magnetometer Ges'), row = 2, col = 1)

    fig.add_trace(go.Scatter(x=df['time'], y=df['rotation_speed_x'],
                        mode='lines',
                        name='Gyro X'), row = 3, col = 1)
    fig.add_trace(go.Scatter(x=df['time'], y=df['rotation_speed_y'],
                        mode='lines',
                        name='Gyro Y'), row = 3, col = 1)
    fig.add_trace(go.Scatter(x=df['time'], y=df['rotation_speed_z'],
                        mode='lines',
                        name='Gyro Z'), row = 3, col = 1)

    fig.add_trace(go.Scatter(x=df['time'], y=df['temperature'],
                        mode='lines',
                        name='Temperature in °C'), row = 4, col = 1)

    fig.add_trace(go.Scatter(x=df['time'], y=df['height'],
                        mode='lines',
                        name='height'), row = 5, col = 1)
    fig.add_trace(go.Scatter(x=df['time'], y=df['height_avg'],
                        mode='lines',
                        name='Smooth height'), row = 5, col = 1)


    fig.update_layout(legend_orientation="v", 
                xaxis5_rangeslider_visible=True, xaxis5_rangeslider_thickness=0.1, 
                plot_bgcolor=colors['background'],
                paper_bgcolor=colors['background'],
                font_color=colors['text'])

    fig.update_yaxes(title_text="accel (g)", row=1, col=1)
    fig.update_yaxes(title_text="magn (uT)", row=2, col=1)
    fig.update_yaxes(title_text="gyro (dps)", row=3, col=1)
    fig.update_yaxes(title_text="temp (°C)", row=4, col=1)
    fig.update_yaxes(title_text="height (in )", row=5, col=1)
    fig.update_layout(margin=dict(t=100))
    return fig

def create_3d_curve(df):
    """ Creates a 3d graph with longitude, latitude and height to visuaize the flight curve."""
    fig = go.Figure(data=[go.Scatter3d( x=df['longitude'], y=df['latitude'], z=df['height_avg'],
                    mode='markers',
                    marker = dict(size = 3, color = df["time"]),
                    hovertext = df["time"],
                    name='Height')])
    fig.update_layout(legend_orientation="v",
                    plot_bgcolor=colors['background'],
                    paper_bgcolor=colors['background'],
                    font_color=colors['text'],
                    )
    fig.update_layout(scene = dict(
                        xaxis = dict(
                            backgroundcolor=colors['background'],
                            gridcolor="white",
                            showbackground=True,
                            zerolinecolor="white",),
                        yaxis = dict(
                            backgroundcolor=colors['background'],
                            gridcolor="white",
                            showbackground=True,
                            zerolinecolor="white"),
                        zaxis = dict(
                            backgroundcolor=colors['3d_zaxis'],
                            gridcolor="white",
                            showbackground=True,
                            zerolinecolor="white",),)
                    )
    fig.update_layout(scene =dict(
                        xaxis_title='Longitude',
                        yaxis_title='Latitude',
                        zaxis_title='Height'),
                    )
    fig.update_layout(margin=dict(l=0, r=0, b=0, t=0))
    fig.update_layout(height=700)
    return fig

def graph_compare(df):
    return html.Div(
        dbc.Row(
            dbc.Card([
                dbc.CardHeader("Sensor Data Comparison"),
                dbc.CardBody(
                    [
                    dcc.Graph(figure=create_sensor_fig(df).update_layout(height=1000, width = 1200))
                    ])
                ],
                style={'background': colors['background']}),
            justify='center'))


def acceleration_fig(df):
    acceleration_fig = go.Figure()
    acceleration_fig.update_layout(title = "Acceleration")
    acceleration_fig.update_yaxes(title_text=("acceleration (g)"))
    acceleration_fig.add_trace(go.Scatter(x=df['time'], y=df['acceleration_x'],
                            mode='lines',
                            name='Acceleration X'))
    acceleration_fig.add_trace(go.Scatter(x=df['time'], y=df['acceleration_y'],
                        mode='lines',
                        name='Acceleration Y'))
    acceleration_fig.add_trace(go.Scatter(x=df['time'], y=df['acceleration_z'],
                        mode='lines',
                        name='Acceleration Z'))
    acceleration_fig.update_layout(legend_orientation="v", 
                plot_bgcolor=colors['background'],
                paper_bgcolor=colors['background'],
                font_color=colors['text'])
    return acceleration_fig

def magnetometer_fig(df):
    magnetometer_fig = go.Figure()
    magnetometer_fig.update_layout(title = "Magnetometer")
    magnetometer_fig.update_yaxes(title_text=("magnetometer (µT)"))
    magnetometer_fig.add_trace(go.Scatter(x=df['time'], y=df['magnetometer_x'],
                            mode='lines',
                            name='Magnetometer X'))
    magnetometer_fig.add_trace(go.Scatter(x=df['time'], y=df['magnetometer_y'],
                        mode='lines',
                        name='Magnetometer Y'))
    magnetometer_fig.add_trace(go.Scatter(x=df['time'], y=df['magnetometer_z'],
                        mode='lines',
                        name='Magnetometer Z'))
    #magnetometer_fig.add_trace(go.Scatter(x=df['time'], y=df['magnetic_variation'],
    #                    mode='lines',
    #                    name='Magnetic Variation'))
    magnetometer_fig.add_trace(go.Scatter(x=df['time'], y=df['magnetometer_norm'],
                        mode='lines',
                        name='Magnetometer Norm'))
    magnetometer_fig.update_layout(legend_orientation="v", 
                plot_bgcolor=colors['background'],
                paper_bgcolor=colors['background'],
                font_color=colors['text'])
    return magnetometer_fig


def rotation_speed_fig(df):
    rotation_speed_fig = go.Figure()
    rotation_speed_fig.update_layout(title = "Gyroscope")
    rotation_speed_fig.update_yaxes(title_text=("rotation speed (°/s)"))
    rotation_speed_fig.add_trace(go.Scatter(x=df['time'], y=df['rotation_speed_x'],
                        mode='lines',
                        name='Gyro X'))
    rotation_speed_fig.add_trace(go.Scatter(x=df['time'], y=df['rotation_speed_y'],
                        mode='lines',
                        name='Gyro Y'))
    rotation_speed_fig.add_trace(go.Scatter(x=df['time'], y=df['rotation_speed_z'],
                        mode='lines',
                        name='Gyro Z'))
    rotation_speed_fig.update_layout(legend_orientation="v", 
                plot_bgcolor=colors['background'],
                paper_bgcolor=colors['background'],
                font_color=colors['text'])
    return rotation_speed_fig

def temperature_fig(df):
    temperature_fig = go.Figure()
    temperature_fig.update_layout(title = "Temperature")
    temperature_fig.update_yaxes(title_text=("temperature (°C)"))
    temperature_fig.add_trace(go.Scatter(x=df['time'], y=df['temperature'],
                        mode='lines',
                        name='Temperature in °C'))
    temperature_fig.update_layout(legend_orientation="v", 
                plot_bgcolor=colors['background'],
                paper_bgcolor=colors['background'],
                font_color=colors['text'])
    return temperature_fig

def height_fig(df):
    height_fig = go.Figure()
    height_fig.update_layout(title = "Height")
    height_fig.update_yaxes(title_text=("height (m)"))
    height_fig.add_trace(go.Scatter(x=df['time'], y=df['height'],
                            mode='lines',
                            name='Height'))
    height_fig.add_trace(go.Scatter(x=df['time'], y=df['height_avg'],
                            mode='lines',
                            name='Smooth height'))
    height_fig.update_layout(legend_orientation="v", 
                plot_bgcolor=colors['background'],
                paper_bgcolor=colors['background'],
                font_color=colors['text'])
    return height_fig

def velocity_fig(df):
    velocity_fig = go.Figure()
    velocity_fig.update_layout(title = "Velocity")
    velocity_fig.update_yaxes(title_text=("velocity (m/s)"))
    velocity_fig.add_trace(go.Scatter(x=df['time'], y=df['speed_z'],
                            mode='lines',
                            name='Velocity Z'))
    velocity_fig.add_trace(go.Scatter(x=df['time'], y=df['speed_z_avg'],
                            mode='lines',
                            name='Smooth Velocity Z'))
    velocity_fig.add_trace(go.Scatter(x=df['time'], y=df['speed_mps'],
                            mode='lines',
                            connectgaps = True,
                            name='Velocity over ground'))
    velocity_fig.add_trace(go.Scatter(x=df['time'], y=df['velocity_total'],
                            mode='lines',
                            connectgaps = True,
                            name='Total Velocity'))
    velocity_fig.update_layout(legend_orientation="v", 
                    plot_bgcolor=colors['background'],
                    paper_bgcolor=colors['background'],
                    font_color=colors['text'])
    return velocity_fig


def graphs(df):
    return html.Div([
            dbc.Row([
                dbc.Col(dbc.Card(dbc.CardBody(dcc.Graph(
                    figure = magnetometer_fig(df)
                )), style={'background': colors['background']}), width=6),
                dbc.Col(dbc.Card(dbc.CardBody(dcc.Graph(
                    figure = acceleration_fig(df)
                )), style={'background': colors['background']}), width=6)
            ], justify='center'),
            dbc.Row([
                dbc.Col(dbc.Card(dbc.CardBody(dcc.Graph(
                    figure = rotation_speed_fig(df), 
                )), style={'background': colors['background']}), width=6),
                dbc.Col(dbc.Card(dbc.CardBody(dcc.Graph(
                    figure = temperature_fig(df)
                )), style={'background': colors['background']}), width=6)
            ], justify='center'),
            dbc.Row([
                dbc.Col(dbc.Card(dbc.CardBody(dcc.Graph(
                    figure = height_fig(df)
                )), style={'background': colors['background']}), width=6),
                dbc.Col(dbc.Card(dbc.CardBody(dcc.Graph(
                    figure = velocity_fig(df)
                )), style={'background': colors['background']}), width=6)
            ], justify='center')
        ])


def map_card(df):
    return html.Div([
    dbc.Row([
        dbc.Col(
            dbc.Card([
                dbc.CardHeader("GPS Data"),
                dbc.CardBody(dcc.Graph(figure = create_map(df)))],
                style={'background': colors['background']}
            ), width = 6
        ),
        dbc.Col(
            dbc.Card([
                dbc.CardHeader("Flight Curve"),
                dbc.CardBody(dcc.Graph(figure = create_3d_curve(df)))],
                style={'background': colors['background']}
            ), width = 6
        )
    ])
])

def general_data(df):
    
    table_header = [
        html.Thead(html.Tr([html.Th("Measurand"),html.Th("Minimum"), html.Th("Maximum"), html.Th("Mean")]))
    ]
    special_vals = []
    for i  in df.columns.values:
        if not i in EXCLUDED_COLUMNS_FOR_GENERAL_DATA: 
            special_vals.append([html.Td(str(df[i].min())), html.Td(str(df[i].max())), html.Td(str(df[i].mean()))])
        else: 
            special_vals.append([html.Td("nan"), html.Td("nan"), html.Td("nan")])

    html_tb = []
    for i in range(len(df.columns)):
        if not i in EXCLUDED_COLUMNS_FOR_GENERAL_DATA: 
            html_tb.append(html.Tr([html.Td(str(df.columns[i])), *special_vals[i]]))
    

    table_body = [html.Tbody(html_tb)]

    table = dbc.Table(table_header + table_body, bordered=True)

    return html.Div(
        dbc.Row(
            dbc.Card([
                dbc.CardHeader("General Data"),
                dbc.CardBody(
                    [
                    table
                    ])
                ],
                style={'background': colors['background']}),
            justify='center'))

#https://stackoverflow.com/questions/63653783/dash-plotly-bootstrap-how-to-design-layout-using-dash-bootstrap-components
# Dropdown for diffrent data files
dropdown = dcc.Dropdown(
            id='dropdown',     
            options=option,
            value=file_paths[len(file_paths)-1],
            searchable = False,
            style= {'width': '20vh', 'color': '#454545'}
            )


def dashboard(df): 
    return html.H4("Hello World!")

home_link = dbc.NavItem(dbc.NavLink("Home", href="/", active="exact"), style={'margin-right': 10})
map_link = dbc.NavItem(dbc.NavLink("Maps", href="/maps", active="exact"), style={'margin-right': 10})
graph_link = dbc.NavItem(dbc.NavLink("Graphs", href="/graphs", active="exact"), style={'margin-right': 10})
graph_comparison_link = dbc.NavItem(dbc.NavLink("Graph Comparison", href="/graph_comparison", active="exact"), style={'margin-right': 10})

# Navbar with logo
navbar = dbc.Navbar(
    dbc.Container(
        [
            html.A(
                # Use row and col to control vertical alignment of logo / brand
                dbc.Row(
                    [
                        dbc.Col(dbc.NavbarBrand("TUDSat - CanSat Flight Data", className="ml-2")),
                    ],
                    align="center",
                    no_gutters=True,
                ),
                href="https://www.tudsat.space/",
            ),
            dbc.NavbarToggler(id="navbar-toggler1"),
            dbc.Collapse(
                dbc.Nav(
                    [home_link, map_link, graph_link, graph_comparison_link, dropdown], className="ml-auto", navbar=True
                ),
                id="navbar-collapse1",
                navbar=True,
            ),
        ]
    ),
    color="dark",
    dark=True,
    className="mb-5",
)


content = html.Div(id="page-content")

app.layout = html.Div(
    [dcc.Location(id = "url"), navbar, content]
)

# Callback for the dropdown 

@app.callback(Output("page-content", "children"), Input("url", "pathname"), dash.dependencies.Input('dropdown', 'value'))
def render_page_content(pathname, value):
    df = pd.read_csv(value)
    if pathname == "/":
        return general_data(df)
    elif pathname == "/maps":
        return map_card(df)
    elif pathname == "/graphs":
        return graphs(df)
    elif pathname == "/graph_comparison":
        return graph_compare(df)
            
    # If the user tries to reach a different page, return a 404 message
    return dbc.Jumbotron(
        [
            html.H1("404: Not found", className="text-danger"),
            html.Hr(),
            html.P(f"The pathname {pathname} was not recognised..."),
        ]
    )


# we use a callback to toggle the collapse on small screens
def toggle_navbar_collapse(n, is_open):
    if n:
        return not is_open
    return is_open


# the same function (toggle_navbar_collapse) is used in all three callbacks

app.callback(
    Output(f"navbar-collapse1", "is_open"),
    [Input(f"navbar-toggler1", "n_clicks")],
    [State(f"navbar-collapse1", "is_open")],
)(toggle_navbar_collapse)

if __name__ == "__main__":
    app.run_server(host="0.0.0.0", debug=False, port=8888, dev_tools_ui=False)
