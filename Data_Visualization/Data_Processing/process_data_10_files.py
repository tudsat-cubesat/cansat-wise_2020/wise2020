import pandas as pd 
import glob
import math
import numpy
import pressure_to_height as c

KNOTS_TO_METERS_PER_SECOND = 0.514444
path = r'./Data_raw/2021-01-29-16-40-33'     # If './sensor_data' doesnt work change it to whole file path
files = glob.glob(path + '/*')

#files.remove('./Data_raw/2021-01-29-16-40-33/gps_raw')
print(files)
dfs = []

# load all raw data files into dataframes
for file in files:
    dfs.append(pd.read_csv(file).set_index('time'))

# eliminate all duplicated indices
for i in range(len(dfs)):
    dfs[i] = dfs[i].loc[~dfs[i].index.duplicated(keep='first')]

# concatenate all dataframes
df = pd.concat(dfs, join='outer', axis=1)

#df.reset_index()
# calculate the height
df["height"] = c.p_to_h_international_reference_height(df["pressure"].iloc[0],df["pressure"])

# calculate velocity in z direction
df["index"] = df.index
df["velocity_z"] = df["height"].diff() / df["index"].diff()
del df["index"]


# norm of magnetic flux density
df["magnetometer_norm"] = numpy.sqrt(df["magnetometer_x"]**2 + df["magnetometer_y"]**2 + df["magnetometer_z"]**2)

# Calculate meters per second from knots
df["velocity_o_g_mps"] = df["knots_over_ground"]*KNOTS_TO_METERS_PER_SECOND

# Calculate x,y,ges-velocity over ground from gps coordinates 
#df["lat_diff"] = df["latitude"].diff(periods = 10)
#df["long_diff"] = df["longitude"].diff(periods = 10)
#df["v_over_ground"] = numpy.sqrt(df['lat_diff']**2 + df['long_diff']**2)

temp = 0
counter = 0
df["velocity_z_avg"] = numpy.nan
for i in df.index:
    temp += df["velocity_z"][i]
    counter += 1
    if df["velocity_o_g_mps"][i] != None:
        df["velocity_z_avg"][i] = temp/counter
        temp = 0
        counter = 0
    #df["velocity_z"]

# calculate total velocity
df["velocity_total"] = numpy.sqrt(df["velocity_o_g_mps"]**2 + df["velocity_z_avg"]**2)

# remove measurement errors from gps data
df["latitude"] = df["latitude"].replace(to_replace=0, value=None)
df["longitude"] = df["longitude"].replace(to_replace=0, value=None)

# Deletes last 10s because of measurement errors
df.drop(df.tail(127).index,inplace=True)
df.to_csv("output10.csv")
print(df.head())
for i in df.columns.values:
    print(i)
    print("Max: ", df[i].max(), "Min:",df[i].min(), "Mean:",df[i].mean())
print()
