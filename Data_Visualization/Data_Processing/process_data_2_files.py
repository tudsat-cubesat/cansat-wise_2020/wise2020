import pandas as pd 
import numpy
import pressure_to_height as c


KNOTS_TO_METERS_PER_SECOND = 0.514444
HEIGHT_AVG_N = 10

DATASET_NAME = "2021-08-29-12-22-58"
DATASET_PREFIX = "second_launch_"
SOURCE_DIRECTORY = "./Data_Processing/data_raw/"
TARGET_DIRECTORY = "./Data_Processing/data_processed/"
#TARGET_DIRECTORY = "./Data_Dashboard/sensor_data/"


path = SOURCE_DIRECTORY + DATASET_NAME + "/"

#converts degrees, decimal minutes to decimal degrees
#
def coordinates_to_dg(n):
    try:
        n_in_gmm = str(float(n))

        deg_n = float(n_in_gmm[0:n_in_gmm.find(".")-2])
        min_n = float(n_in_gmm[n_in_gmm.find(".")-2:])

        n_in_dg = deg_n + min_n/60

        n_in_dg = round(n_in_dg, 6)

        return n_in_dg
    except:
        return n


# def latitude_to_dg(n):
#     try:
#         n_in_gmm = str(float(n)/100)

#         deg_n = float(n_in_gmm.split(".")[0])
#         min_n = float(n_in_gmm.split(".")[1])

#         n_in_dg = deg_n + min_n/60

#         n_in_dg = round(n_in_dg, 6)

#         return n_in_dg
#     except:
#         return n

def smooth(y, box_pts):
    box = numpy.ones(box_pts)/box_pts
    y_smooth = numpy.convolve(y, box, mode='same')
    return y_smooth


if __name__ == "__main__":
    # Dataframes of the Neo 6m GPS module and the BMP280 and MPU0240 sensorboards

    neo_6m = pd.read_csv(path+"neo_6m.csv", sep = ",")  
    bmp_mpu = pd.read_csv(path+"bmp_mpu.csv", sep = ",")


    # Rounding the time to two decimal places 
    neo_6m["time"] = neo_6m["time"].round(2)
    bmp_mpu["time"] = bmp_mpu["time"].round(2)

    #Setting the index to the time 
    neo_6m = neo_6m.set_index("time")
    bmp_mpu = bmp_mpu.set_index("time")

    # Changing longitude/latitude format
    neo_6m["longitude"] = neo_6m["longitude"].apply(lambda n: coordinates_to_dg(n))
    neo_6m["latitude"] = neo_6m["latitude"].apply(lambda n: coordinates_to_dg(n))


    # Merging the two dataframes and sorting them
    df_merged = pd.concat([neo_6m, bmp_mpu])
    df_merged = df_merged.sort_index()

    # Merging two rows with the same index
    df_merged = df_merged.groupby(["time"]).first()

    # Fill missing values forward
    df = df_merged.interpolate()
    #df = df_merged

    # Calculating...
    # speed from knots to meters per second 
    df["speed"] = df["speed"]/1000
    df["speed_mps"] = df["speed"]*KNOTS_TO_METERS_PER_SECOND

    # # the height from the air pressure in hPa
    df["height"] = c.p_to_h_international_reference_height(df["pressure"].iloc[0],df["pressure"])

    df["height_avg"] = smooth(df["height"], 40)

    df.reset_index(inplace=True)

    # # the speed in z direction
    df["speed_z"] = df["height_avg"].diff() / df["time"].diff()
    df["speed_z_avg"] = smooth(df["speed_z"], 40)


    # df = df.reset_index()
    # df["speed_z"] = df["height_avg"].diff() / df["time_avg"].diff()
    # df["speed_z"] = df["speed_z"].fillna(method = "bfill")

    # the norm of magnetic flux density
    df["magnetometer_norm"] = numpy.sqrt(df["magnetometer_x"]**2 + df["magnetometer_y"]**2 + df["magnetometer_z"]**2)

    # total velocity
    df["velocity_total"] = numpy.sqrt(df["speed_mps"]**2 + df["speed_z_avg"]**2)


    # Saving the processed datafram as csv file
    df.to_csv( TARGET_DIRECTORY + DATASET_PREFIX + DATASET_NAME + ".csv")
