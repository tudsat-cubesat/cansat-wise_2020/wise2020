from pressure_to_height import *

with open("./2021-01-11-15-24-57/pressure", "r") as file:
    r = file.readlines()
    p_0 = r[0].split(",")[1]
    with open("./2021-01-11-15-24-57/height", "a") as file_out:
        for line in r:
            data = line.split(",")
            file_out.write(str(data[0])+","+ str(p_to_h_international_reference_height(float(p_0), float(data[1])))+"\n")

with open("./2021-01-11-15-24-57/magnetometer_x", "r") as file_mx, open("./2021-01-11-15-24-57/magnetometer_y", "r") as file_my, open("./2021-01-11-15-24-57/magnetometer_z", "r") as file_mz:
    rx = file_mx.readlines()
    ry = file_my.readlines()
    rz = file_mz.readlines()
    with open("./2021-01-11-15-24-57/magnetometer_ges", "a") as file_out:
        for line in rx:
            datax = line.split(",")
            datay = line.split(",")
            dataz = line.split(",")
            file_out.write(str(datax[0])+","+ str(float(datax[1])+float(datay[1])+float(dataz[1]))+"\n")