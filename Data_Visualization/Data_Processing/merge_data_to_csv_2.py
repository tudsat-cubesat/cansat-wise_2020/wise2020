import pandas as pd 

dfs = []
files = ['acceleration_x', 'acceleration_y', 'acceleration_z', 'magnetometer_x', 'magnetometer_y', 'magnetometer_z', 
    'rotation_speed_x', 'rotation_speed_y', 'rotation_speed_z', 'pressure', 'height', 'temperature', 'Longitude', 'Latitude']
for i in range(len(files)):
    dfs.append(pd.read_csv("./2021-01-11-15-24-57/" + files[i] , index_col=[0]))
for i in range(len(dfs)):
    dfs[i] = dfs[i].loc[~dfs[i].index.duplicated(keep='first')]

df = pd.concat(dfs, join='outer', axis=1)

df.to_csv("output.csv")
print(df)

    
    