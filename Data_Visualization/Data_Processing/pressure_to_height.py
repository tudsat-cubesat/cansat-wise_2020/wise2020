import math

def p_to_h_international(p):
    ''' Calculates the height from pressure over sea level with the international height formular.
    
    T_0 = 288.15 K
    p_0 = 1013.25 hPa
    temperature gradient = 0.65 per 100m
    valid to 11km altitude
    '''
    return (288.15/0.0065)*(1-(p/1013.25)**(1/5.255))

def p_to_h_international_reference_height(p_0, p):
    ''' Calculates the height from pressure over reference height h_0 with the international height formular.
    
    T_0 = 288.15 K
    temperature gradient = 0.65 per 100m
    valid to 11km altitude
    '''
    return (288.15/0.0065)*(1-(p/p_0)**(1/5.255))