# Calculating distance between two coordinates with haversine formula
import math as m

R = 6371000 # average earth radius in meter
# Latitude and Longitude for poits A and B
A_lat = 49.876085
A_lon = 8.66432
B_lat = 49.876094
B_lon = 8.664201    
#distance between the specified points = 8.63 m (this program) = 8.61 (Google Maps)
def radian_measure(deg):
    """ Converts angle in degrees to radians """
    return (m.pi * deg)/180

dlon = B_lon - A_lon
dlat = B_lat - A_lat

a = (m.sin(radian_measure(dlat/2)))**2 + m.cos(radian_measure(A_lat)) * m.cos(radian_measure(B_lat)) * (m.sin(radian_measure(dlon/2)))**2
c = 2 * m.asin(min(1,m.sqrt(a))) 
d = R * c

print(d)
