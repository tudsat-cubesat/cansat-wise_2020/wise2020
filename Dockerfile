FROM python:3.9

# install needed python packages
COPY requirements.txt .
RUN pip install -r requirements.txt

WORKDIR /app
COPY Data_Visualization/Data_Dashboard .

EXPOSE 8888
HEALTHCHECK CMD curl --fail http://localhost:8888 || exit 1

# run the dashboard
CMD python app.py
