import logging
import sys
sys.path.append("/home/pi/wise2020/Raspberry Pi")
sys.path.append("/home/pi/wise2020/Raspberry Pi/Sensors")
sys.path.append("/home/pi/.local/lib/python3.7/site-packages")
import mpu9250
import bmp280
import neo_6m
import datetime
import math
import time
import os
import threading
import subprocess
import RPi.GPIO as GPIO
import shutil

GPIO.setmode(GPIO.BCM)
GPIO.setup(4, GPIO.IN)

logging.basicConfig(filename='/home/pi/wise2020/Raspberry Pi/logs.log', level=logging.INFO) #log everything in /home/pi/wise2020/Raspberry Pi/logs.log

def main():
	now = datetime.datetime.now()
	timestamp = str(now.strftime("%Y-%m-%d-%H-%M-%S"))
	global start_time
	start_time = time.time()
	global run
	run = True

	logging.info(f" started main() at {timestamp}")

	power = GPIO.input(4) #Checks for power source for testing purposes. False if battery voltage is low or battery disconnected
	if power:
		logging.info(" Running on battery power")
	else:
		logging.info(" Running on usb power")

	global path
	path = "/home/pi/wise2020/Raspberry Pi/sensor_data/" + timestamp

	try:
		os.mkdir(path) #create folder for data files
	except:
		logging.warning(f" Creation of the directory {path} failed")

	create_data_files()

	mpu_bmp = threading.Thread(target = mpu_bmp_thread, args = (), daemon = True) #fork mpu and bmp data collection into extra thread
	video_command = 'raspivid -t 10800000 -sg 600000 -o "' + path + '/video%s.h264" &' #for 3 hours of 10 minute videos use -t 10800000 -sg 600000

	sensors_started = False
	video_started = False
	termination_count = 0
	while True:
		try: #start sensor readout if failed before
			if not sensors_started:
				mpu_bmp.start()
				sensors_started = True
		except KeyboardInterrupt:
			logging.warning("KeyboardInterrupt")
			print("KeyboardInterrupt")
			break
		except:
			logging.warning("Error while starting mpu_bmp thread")

		try: #start video feed if failed before
			if not video_started:
				logging.info(" cmd: " + video_command)
				os.system(video_command)
				logging.info(" started video recording with raspivid")
				video_started = True
		except KeyboardInterrupt:
			logging.warning("KeyboardInterrupt")
			print("KeyboardInterrupt")
			break
		except:
			logging.warning("Error while starting camera feed")

		try: #save neo data(GPS)
			with open(path + "/neo_6m.csv", "a") as file:
				file.write(str(time.time() - start_time) + "," + neo_6m.get_data() + "\n") #takes ~1 second
		except KeyboardInterrupt:
			logging.warning("KeyboardInterrupt")
			print("KeyboardInterrupt")
			break
		except:
			logging.exception(" Error while fetching neo data")
			time.sleep(1)

		try:
			disk_space = shutil.disk_usage(path)[2]
			print(disk_space)
			if disk_space < 1024*1024*500: #True if less than 500MB available
				logging.warning(f"low disk space in 'sensor_data'. Less than {math.floor(float(disk_space)/(1024*1024))}MB available. Program terminated")
				break
		except:
			logging.exception(" Unable to check free disk space")

		try: #test for disconnected sensors and low battery voltage and power source and terminate program after 5 consecutive errors.
			if not detect_i2c(68):
				logging.warning(" Lost connection to Sensors")
			if GPIO.input(4) != power:
				if termination_count >= 5:
					logging.warning(" Program terminated")
					break
				else:
					logging.warning(" Low battery voltage")
					termination_count += 1
			else:
				termination_count = 0
		except KeyboardInterrupt:
			logging.warning("KeybaordInterrupt")
			print("KeyboardInterrupt")
			break
		except:
			logging.exception("")

	run = False #terminates bmp_mpu thread

	time.sleep(3)

	return(True)


def create_data_files():
	'''create data files'''
	try:
		with open(path + "/neo_6m.csv", "a") as file:
			file.write("time,utc_time,NRW,latitude,NS,longitude,EW,speed,course,date,magVar,magEW,checksum\n")
	except:
		logging.exception("")

	try:
		with open(path + "/bmp_mpu.csv", "a") as file:
			file.write("time,acceleration_x,acceleration_y,acceleration_z,rotation_speed_x,rotation_speed_y,rotation_speed_z,magnetometer_x,magnetometer_y,magnetometer_z,temperature,pressure\n")
	except:
		logging.exception("")


def detect_i2c(port):
	'''check if sensor is connected at given i2c port'''
	try:
		output = str(subprocess.Popen('sudo i2cdetect -y 1', shell=True, stdout=subprocess.PIPE).communicate()[0])
		return(str(port) in output)
	except:
		logging.exception("")
		return(False)


def mpu_bmp_thread():
	logging.info(" started mpu_bmp thread")
	buffer = ""
	n = 0
	while run:
		try:
			buffer += str(time.time() - start_time) + "," +  mpu9250.get_data() #mpu9250.get_data() returns data string seperated by commata like: "0.105,-0.01,-1.03,...,-72.824"
		except:
			buffer += str(time.time() - start_time) + ",,,,,,,,,," #if there is an error, we save a bunch of commata for processing purposes
			logging.warning(" Error while fetching mpu data")

		try:
			buffer += "," + bmp280.get_data() + "\n" #bmp280.get_data() returns data string seperated by commata like "30.43,1009.5848"
		except:
			buffer += ",,\n"
			logging.exception(" Error while fetching bmp data")

		n += 1

		if n == 100: #not every line is instantly saved to file. Instead we store n lines and save them all together to reduce cpu usage
			try:
				with open(path + "/bmp_mpu.csv", "a") as file:
					file.write(buffer)
				buffer = ""
				n = 0
			except:
				logging.exception(" Error while saving mpu and bmp data")


if __name__ == "__main__":
	main()
