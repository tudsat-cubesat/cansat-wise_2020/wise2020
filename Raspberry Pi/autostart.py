#executed on startup. Change in /etc/rc.local

import os
import sys
sys.path.append("/home/pi/wise2020/Raspberry Pi/Sensors")
sys.path.append("/home/pi/wise2020/Raspberry Pi")
import logging
import time
import datetime
import RPi.GPIO as GPIO
import threading
import shutil
import math

time.sleep(60) #wait 60 seconds to ensure autostart doesn't mess up the entire system

GPIO.setmode(GPIO.BCM)
GPIO.setup(4, GPIO.IN)

if GPIO.input(4): #check if battery is connected
	now = datetime.datetime.now()
	timestamp = str(now.strftime("%Y-%m-%d-%H-%M-%S"))

	logging.basicConfig(filename='/home/pi/wise2020/Raspberry Pi/logs.log', level=logging.INFO) #log everything in /home/pi/wise2020/Raspberry Pi/logs.log
	logging.info(f"\n\n\n\n\nautostart at {timestamp}\n\n\n\n")

	try:
		logging.info(f"free disk space in sensor_data: {math.floor(float(shutil.disk_usage('/home/pi/wise2020/Raspberry Pi/sensor_data')[2])*100/(1024*1024*1024))/100}GB")
		#output free disk space in sensor_data in GB with two decimal places
	except:
		logging.info(f"disk usage: {shutil.disk_usage}")

	logging.info("fetching new time from GPS-module")


	n = 0
	while n <= 60: #use n <= 60 for flight
		try: #update Raspberry Pi time. Necessary because the Raspberry Pi has no internal battery
			import neo_6m
			date = neo_6m.get_time()
			os.system('sudo date -s ' + '"' + date + '"')
			logging.info(f" changed Raspberry Pi time to {date}")
			break
		except:
			logging.warning("Error while getting time from GPS")
			time.sleep(1)
			n += 1

	while True:
		try:
			from main import main
			if main(): # run main.py
				logging.warning(f' main() returned True, shutdown')
				os.system("shutdown now") #activate before flight
				print("shutdown")
				break
		except KeyboardInterrupt:
			break
		except:
			logging.exception("")
			time.sleep(1)
			break
else:
	print("USB cable connected. Did not start main.py")
	logging.info("USB cable connected. Didn't start main.py")
