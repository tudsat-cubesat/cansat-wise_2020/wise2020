import serial
import string
import pynmea2
import time

def raw_data():
	'''returns $GPRMC line of data from GPS-Module'''
	for i in range(20):
		port="/dev/ttyAMA0"
		ser=serial.Serial(port, baudrate=9600, timeout=0.5)
		dataout = pynmea2.NMEAStreamReader()
		newdata=ser.readline()

		if newdata[0:6] == b'$GPRMC':
			return(newdata)

'''
def gmm_to_dg(data):
	converts degrees, decimal minutes to decimal degrees

	if data[0] != 0 and data[0] != None and data[0] != "":
		lat_in_gmm = str(data[0])
		lng_in_gmm = str(data[1])

		deg_lat = float(lat_in_gmm[0:2])
		min_lat = float(lat_in_gmm[2:])
		deg_lng = float(lng_in_gmm[0:2])
		min_lng = float(lng_in_gmm[2:])

		lat_in_dg = deg_lat + min_lat/60
		lng_in_dg = deg_lng + min_lng/60

		lat_in_dg = round(lat_in_dg, 6)
		lng_in_dg = round(lng_in_dg, 6)

		if str(data[2]) == "S":
			lat_in_dg = -lat_in_dg

		if str(data[3]) == "W":
			lng_in_dg = -lng_in_dg

		return (lat_in_dg, lng_in_dg)

	else:
		return (0, 0)
'''

def get_time():
	'''returns time string (e.g. "16 Jan 2021 16:26:42")'''
	line = raw_data()
	data = str(line)
	data = data.split(",")
	time = data[1]
	date = data[9]
	months = ["", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dec"]
	month = months[int(date[2:4])]
	if time[0:2] != 23:
		return(date[0:2] + " " + month + " 20" + date[4:6] + " " + str(int(time[0:2]) + 1) + ":" + time[2:4] + ":" + time[4:6])
	else:
		return(date[0:2] + " " + month + " 20" + date[4:6] + " 00" + ":" + time[2:4] + ":" + time[4:6])

def get_data():
	'''returns raw data from GPS-Module and the refined coordinates, if existing'''
	line = raw_data()
	#data = str(line)
	#data = data.split(",")

	#gps_time = data[1]
	#lat = data[3]
	#NS = data[4]
	#lng = data[5][1:]
	#EW = data[6]
	#knots_over_ground = data[7]
	#course = data[8]
	#magVar = data[10]
	#magEW = data[11]

	#lat, lng = gmm_to_dg((lat, lng, NS, EW))
	#if magEW == "W":
	#	magVar = -1 * int(magVar)

	#return ("latitude", lat), ("longitude", lng), ("gps_raw", str(line)), ("utc_time", gps_time), ("knots_over_ground", knots_over_ground), ("course_made_good", course), ("magnetic_variation", magVar)
	#return str(lat) + "," + str(lng) + "," + str(line) + "," + gps_time + "," + knots_over_ground + "," + course + "," + str(magVar)
	return str(line)[9:-5]

if __name__ == "__main__":
	while True:
		print(get_data())
